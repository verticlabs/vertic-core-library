var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	del = require('del');

gulp.task('clean', function (cb) {
	del(['dist/*.js']);
	return cb();
});

gulp.task('build', ['clean'], function(file) {
	//
	var output = gulp
		.src('src/**/*.js')
		.pipe(uglify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('dist/'));
});

gulp.task('default', ['build']);
