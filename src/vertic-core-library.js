/*
* Vertic Core Library
*	MIT License
*
* ----------------------------------
* Examples of use:
* ----------------------------------
*
* var Human = Core.extend({
* 	init: function (height, weight){
* 		this.height = height;
* 		this.weight = weight;
* 	}
* });
* var Mutant = Human.extend({
* 	init: function init(height, weight, abilities){
* 		init.base.call(this, height, weight);
* 		this.abilities = abilities;
* 	}
* });
*
* var theWolverine = new Mutant('5ft 3in', 300, [
* 	'adamantium skeleton',
* 	'heals quickly',
* 	'claws'
* ]);
*/

(function () {
	var initializing = false;

	// The base Class implementation (does nothing)
	this.Core = function () {};

	// Create a new Class that inherits from this class
	Core.extend = function extend(prop) {
		var _super = this.prototype;

		// Instantiate a base class (but only create the instance,
		// don't run the init constructor)
		initializing = true;
		var prototype = new this();
		initializing = false;

		// Copy the properties over onto the new prototype
		for (var name in prop) {
			// if we're overwriting an existing function
			// set the base property
			var value = prop[name];

			if (typeof prop[name] == 'function' && typeof _super[name] == 'function'){
				value.base = _super[name];
			}

			prototype[name] = value;
		}

		// The dummy class constructor
		function CoreObject() {
			// All construction is actually done in the init method
			if (!initializing && this.init)
				this.init.apply(this, arguments);
		}

		// Populate our constructed prototype CoreObject
		CoreObject.prototype = prototype;

		// Enforce the constructor to be what we expect
		CoreObject.prototype.constructor = CoreObject;

		CoreObject.prototype.channels = {};

		CoreObject.prototype.trigger = function(channel) {
			if (!this.channels[channel]) return false;

			var args = Array.prototype.slice.call(arguments, 1);

			for (var i = 0, l = this.channels[channel].length; i < l; i++) {
				var subscription = this.channels[channel][i];
				subscription.callback.apply(subscription.context, args);
			}

			return this;
		};

		CoreObject.prototype.on = function(channel, fn) {
			if (!this.channels[channel]) this.channels[channel] = [];
			this.channels[channel].push({ context: this, callback: fn });

			return this;
		};

		CoreObject.prototype.off = function(channel) {
			if (this.channels[channel]) delete this.channels[channel];
		};

		// And make this CoreObject extendable
		CoreObject.extend = arguments.callee;

		return CoreObject;
	};

	// Exports
	root = typeof exports !== 'undefined' && exports !== null ? exports : this;

	root.Core = Core;

	return Core;
})();
